## README ER550 ##

# Introduction #
ER550 Board is developed by EasyReach Solutions Pvt. Ltd. (http://easyreach.co.in). It uses S76S AcSiP LoRa® module and Nuvoton NANO100NC2BN MCU. 

ER550 Board Pictures
![ER550](img/pcb.png)

# How to Program #
![ER550](img/How_To_Prog.jpeg)
You'll need Keil or IAR setup on your machine. In the following sample codes we've used Keil as an IDE.<br>
In the image above you can find ER550 attached to a Nuvoton NU-Link ICE Debugger. You need to plug this into your machine and isntall the following driver:<br>
http://www.nuvoton.com/hq/resource-download.jsp?tp_GUID=SW0520101208200142 <br>
Now just open any of the following sample projects and Keil will install additional software packs required for Nuvoton Cortex M0 MCUs itself.<br>
Build all the target files (F7) and flash the chip (F8).<br>

## Connection details ##
![ER550](img/er550_usb_connections.png)


# Sample Projects #
1. Blinky (https://gitlab.com/easyreach.iot.public/Blink.git)
2. Hello LoRa (https://gitlab.com/easyreach.iot.public/Hello_LoRa.git)
3. Single Tap Wakeup (https://gitlab.com/easyreach.iot.public/Single_Tap_Wakeup.git)
4. Nuvoton Libraries-This is required dependency for all the projects (https://gitlab.com/easyreach.iot.public/nuvoton.git)

# How to get ER550? #
To find instruction on how to purchase, visit http://easyreach.co.in/purchase/

# Applications #
1. Use ER550A as a beacon. It wakes up at a regular interval to send information about its availibility on LoRa. E.g. in plats seperated by large distance, one can check if the truck or machines is in witch plant
2. User accelerometer to wakeup the board and send data immediately to Gateway. E.g. send data only when Truck or machine is in motion and send data over LoRa
3. Read Water Flow Pulses in water meter using PIN and send data over LoRa
